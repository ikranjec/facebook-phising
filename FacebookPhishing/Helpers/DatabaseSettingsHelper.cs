﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Helpers {
    public class DatabaseSettingsHelper {
        public DatabaseSettingsHelper() {

        }

        public string UserId { get; set; }

        public string Password { get; set; }

        public string Host { get; set; }

        public string Port { get; set; }

        public string Database { get; set; }

        public override string ToString() {
            return $"User ID={UserId}; Password={Password}; Host={Host}; Port={Port}; Database={Database}";
        }
    }
}
