﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Constants {
    public static class UserCsvHeaders {
        public const string ID = "ID";
        public const string EMAIL = "EMAIL";
        public const string PASSWORD = "PASSWORD";
    }
}
