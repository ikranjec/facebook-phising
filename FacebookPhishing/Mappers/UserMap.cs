﻿using CsvHelper.Configuration;
using FacebookPhishing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Mappers {
    public sealed class UserMap : ClassMap<User> {
        public UserMap() {
            Map(m => m.Id).Name(Constants.UserCsvHeaders.ID);
            Map(m => m.Email).Name(Constants.UserCsvHeaders.EMAIL);
            Map(m => m.Password).Name(Constants.UserCsvHeaders.PASSWORD);
        }
    }
}
