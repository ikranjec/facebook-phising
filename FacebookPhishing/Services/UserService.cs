﻿using FacebookPhishing.Data.Context;
using FacebookPhishing.Interfaces;
using FacebookPhishing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Services {
    public class UserService : IUserService {
        public UserService(FacebookPhisingDbContext dbContext) {
            _dbContext = dbContext;
        }

        public void Create(User user) {
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
        }

        public IEnumerable<User> GetAll() {
            return (
                from a in _dbContext.Users
                orderby a.Id
                select a
            ).ToArray();
        }

        public FacebookPhisingDbContext dbContext { get => _dbContext; }

        private readonly FacebookPhisingDbContext _dbContext;

    }
}
