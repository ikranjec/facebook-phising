using FacebookPhishing.Data.Context;
using FacebookPhishing.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FacebookPhishing {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
            _databaseSettingsHelper = new DatabaseSettingsHelper();
            DatabaseSettingsConfig();
        }

        public IConfiguration Configuration { get; }
        private DatabaseSettingsHelper _databaseSettingsHelper { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddCors();

            services.AddControllersWithViews();

            services.AddDbContext<FacebookPhisingDbContext>(opt => opt.UseNpgsql(_databaseSettingsHelper.ToString()));

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration => {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment()) {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

        private void DatabaseSettingsConfig() {
            // Database config 
            _databaseSettingsHelper.UserId = Configuration.GetSection("ConnectionStrings:PostgreSQL:UserId").Value;
            _databaseSettingsHelper.Password = Configuration.GetSection("ConnectionStrings:PostgreSQL:Password").Value;
            _databaseSettingsHelper.Host = Configuration.GetSection("ConnectionStrings:PostgreSQL:Host").Value;
            _databaseSettingsHelper.Port = Configuration.GetSection("ConnectionStrings:PostgreSQL:Port").Value;
            _databaseSettingsHelper.Database = Configuration.GetSection("ConnectionStrings:PostgreSQL:Database").Value;
        }
    }
}
