﻿using FacebookPhishing.Data.Context;
using FacebookPhishing.Models;
using FacebookPhishing.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace FacebookPhishing.Controllers {
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller {
        public UserController(FacebookPhisingDbContext dbContext) {
            _dbContext = dbContext;
            _userService = new UserService(_dbContext);
        }

        [HttpPost]
        public void CreateUser([FromBody] User user) {
            _userService.Create(user);
        }

        private readonly FacebookPhisingDbContext _dbContext;
        private readonly UserService _userService;
    }
}
