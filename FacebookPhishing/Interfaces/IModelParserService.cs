﻿using FacebookPhishing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Interfaces {
    public interface IModelParserService<T> {
        void ExportToCSV(string path, IEnumerable<T> modelList);
    }
}
