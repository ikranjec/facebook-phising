﻿using FacebookPhishing.Data.Context;
using FacebookPhishing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Interfaces {
    public interface IUserService {
        void Create(User user);
        IEnumerable<User> GetAll();

        FacebookPhisingDbContext dbContext { get; }
    }
}
