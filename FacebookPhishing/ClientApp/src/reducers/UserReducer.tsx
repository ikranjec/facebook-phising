﻿// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.


import { Action, Reducer } from "redux";
import { UserLoginAction } from "../actions/UserActions";
import ActionTypes from "../action_types/ActionTypes";
import { UserState } from "../state_stores/UserState";
import { InitialUserItem } from '../types/InitialUserItem'


export const userReducer: Reducer<UserState> = (state: UserState | undefined, incomingAction: Action): UserState => {
    const action = incomingAction as UserLoginAction;
    switch (action.type) {
        case ActionTypes.USER_LOGIN:
            return { isLoading: false, user: action.payload }
        default:
            return { isLoading: false, user: InitialUserItem }
    }
};