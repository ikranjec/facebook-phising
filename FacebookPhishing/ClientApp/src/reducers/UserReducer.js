"use strict";
// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.
Object.defineProperty(exports, "__esModule", { value: true });
exports.userReducer = void 0;
var ActionTypes_1 = require("../action_types/ActionTypes");
var InitialUserItem_1 = require("../types/InitialUserItem");
var userReducer = function (state, incomingAction) {
    var action = incomingAction;
    switch (action.type) {
        case ActionTypes_1.default.USER_LOGIN:
            console.log(action);
            return { isLoading: false, user: action.payload };
        default:
            return { isLoading: false, user: InitialUserItem_1.InitialUserItem };
    }
};
exports.userReducer = userReducer;
//# sourceMappingURL=UserReducer.js.map