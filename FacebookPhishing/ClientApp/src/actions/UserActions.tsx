﻿import IUser from "../types/IUser";
import ActionTypes from "../action_types/ActionTypes";

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

export interface UserLoginAction {
    type: ActionTypes.USER_LOGIN,
    payload: IUser
}
