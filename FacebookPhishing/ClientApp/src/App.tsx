import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Login from './components/Login';

import './styles/main.scss'

export default () => (
    <Layout>
        <Route exact path='/' component={Login} />
    </Layout>
);
