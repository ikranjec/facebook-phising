﻿import * as React from 'react';
import { connect } from 'react-redux';
import IUser from '../types/IUser'
import { actionCreators } from '../action_creators/UserActionCreator'
import { InitialUserItem } from '../types/InitialUserItem'
import { FC } from 'react';
import { ApplicationState } from '../store';
import { UserState } from '../state_stores/UserState';
import { RouteComponentProps } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';

type UserProps =
    UserState &
    typeof actionCreators &
    RouteComponentProps<{}>;

const Login: FC<UserProps> = (props) => {
    const [user, setUser] = React.useState<IUser>(InitialUserItem)

    const handleChange = (event: any) => {
        const { name, value } = event.target;
        setUser(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const isEmpty = (word: string): boolean => {
        return (!word || word.length === 0);
    }

    const handleSubmit = () => {
        if (!isEmpty(user.email) && !(isEmpty(user.password))) {
            props.login(user)
        } else {
            NotificationManager.warning('Warning!', 'Please enter your credentials correctly!');
        }
        NotificationManager.info('Info message');
    }

    return (
        <div className="container">
            <div className="row align-items-center justify-content-center vh-100">
                <div className="col-md-7">
                    <img src={process.env.REACT_APP_LOGO} className="w-50" />
                    <h3>Facebook helps you connect and share with the people in your life.</h3>
                </div>
                <div className="col-md-5">
                    <div className="login-form" >
                        <div className="mb-3">
                            <input type="email" className="form-control" placeholder="Email address or phone number" name="email"
                                required onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <input type="password" className="form-control" placeholder="Password" name="password"
                                required onChange={handleChange} />
                        </div>
                        <button type="submit" className="btn btn-custom btn-lg mt-3" onClick={() => handleSubmit()}>Login</button>
                    </div>
                    <p className="pt-3 text-center"><b>Create a Page</b> for a celebrity, band or business.</p>
                </div>
            </div>
        </div>
    )
}

export default connect((state: ApplicationState) => state.userState, actionCreators)(Login as any);