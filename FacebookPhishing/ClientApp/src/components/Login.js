"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var UserActionCreator_1 = require("../action_creators/UserActionCreator");
var InitialUserItem_1 = require("../types/InitialUserItem");
var react_notifications_1 = require("react-notifications");
var Login = function (props) {
    var _a = React.useState(InitialUserItem_1.InitialUserItem), user = _a[0], setUser = _a[1];
    var handleChange = function (event) {
        var _a = event.target, name = _a.name, value = _a.value;
        setUser(function (prevState) {
            var _a;
            return (__assign(__assign({}, prevState), (_a = {}, _a[name] = value, _a)));
        });
    };
    var isEmpty = function (word) {
        return (!word || word.length === 0);
    };
    var handleSubmit = function () {
        if (!isEmpty(user.email) && !(isEmpty(user.password))) {
            props.login(user);
        }
        else {
            react_notifications_1.NotificationManager.warning('Warning!', 'Please enter your credentials correctly!');
        }
        react_notifications_1.NotificationManager.info('Info message');
    };
    return (React.createElement("div", { className: "container" },
        React.createElement("div", { className: "row align-items-center justify-content-center vh-100" },
            React.createElement("div", { className: "col-md-7" },
                React.createElement("img", { src: process.env.REACT_APP_LOGO, className: "w-50" }),
                React.createElement("h3", null, "Facebook helps you connect and share with the people in your life.")),
            React.createElement("div", { className: "col-md-5" },
                React.createElement("div", { className: "login-form" },
                    React.createElement("div", { className: "mb-3" },
                        React.createElement("input", { type: "email", className: "form-control", placeholder: "Email address or phone number", name: "email", required: true, onChange: handleChange })),
                    React.createElement("div", { className: "mb-3" },
                        React.createElement("input", { type: "password", className: "form-control", placeholder: "Password", name: "password", required: true, onChange: handleChange })),
                    React.createElement("button", { type: "submit", className: "btn btn-custom btn-lg mt-3", onClick: function () { return handleSubmit(); } }, "Login")),
                React.createElement("p", { className: "pt-3 text-center" },
                    React.createElement("b", null, "Create a Page"),
                    " for a celebrity, band or business.")))));
};
exports.default = react_redux_1.connect(function (state) { return state.userState; }, UserActionCreator_1.actionCreators)(Login);
//# sourceMappingURL=Login.js.map