﻿import IUser from "../types/IUser";

// -----------------
// STATE - This defines the type of data maintained in the Redux store.
export interface UserState {
    isLoading: boolean;
    user: IUser
}