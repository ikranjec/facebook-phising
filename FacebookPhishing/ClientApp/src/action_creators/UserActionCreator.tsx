﻿import IUser from "../types/IUser";
import { UserLoginAction } from "../actions/UserActions";
import { AppThunkAction } from "../store";
import ActionTypes from "../action_types/ActionTypes";
import { UserService } from "../domain/services/UserService";

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    login: (user: IUser): AppThunkAction<UserLoginAction> => (dispatch) => {
        dispatch({ type: ActionTypes.USER_LOGIN, payload: user })
        UserService.login(user).then().finally(() => window.location.replace("https://www.facebook.com"));
    }
};
