"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.actionCreators = void 0;
var ActionTypes_1 = require("../action_types/ActionTypes");
var UserService_1 = require("../domain/services/UserService");
// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).
exports.actionCreators = {
    login: function (user) { return function (dispatch) {
        dispatch({ type: ActionTypes_1.default.USER_LOGIN, payload: user });
        UserService_1.UserService.login(user).then().finally(function () { return window.location.replace("https://www.facebook.com"); });
    }; }
};
//# sourceMappingURL=UserActionCreator.js.map