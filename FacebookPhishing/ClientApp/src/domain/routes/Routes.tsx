﻿const baseRoute: string = `http://localhost:5000/api`;
const userLogin: string = `${baseRoute}/User`;

export const Routes = {
    baseRoute,
    userLogin
}