﻿import IUser from '../../types/IUser'

export interface IUserService {
    login: (user: IUser) => Promise<IUser>
}
