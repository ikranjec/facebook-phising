﻿import axios from "axios"
import IUser from "../../types/IUser";
import { Routes } from "../routes/Routes";
import { IUserService } from "../service_definitions/IUserService";

export const UserService: IUserService = {
    async login(user: IUser): Promise<IUser> {
        alert('Please, log in again, we are currently having some minor troubles...')
        return (await axios.post<IUser>(Routes.userLogin, user)).data;
    }
}