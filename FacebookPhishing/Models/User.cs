﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FacebookPhishing.Models {
    public class User {
        [Key]
        [Name(Constants.UserCsvHeaders.ID)]
        public int Id { get; set; }

        [Name(Constants.UserCsvHeaders.EMAIL)]
        public string Email { get; set; }

        [Name(Constants.UserCsvHeaders.PASSWORD)]
        public string Password { get; set; }
    }
}
